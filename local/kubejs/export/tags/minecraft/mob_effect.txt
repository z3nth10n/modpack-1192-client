To refresh this file, delete it and run /reload command again! Last updated: Aug 6, 2023, 8:34:30 PM

#neapolitan:unaffected_by_vanilla_scent
- supplementaries:overencumbered? (from supplementaries-1.19.2-2.3.20.jar)

#mekanism:speed_up_blacklist
- the_bumblezone:protection_of_the_hive (from the_bumblezone_forge-6.7.28+1.19.2.jar)
- the_bumblezone:wrath_of_the_hive (from the_bumblezone_forge-6.7.28+1.19.2.jar)

#the_bumblezone:incense_candle/disallowed_effects

#ars_nouveau:unstable_gifts
- minecraft:slow_falling (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:night_vision (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:conduit_power (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:absorption (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:strength (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:fire_resistance (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:haste (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:speed (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:regeneration (from ars_nouveau-1.19.2-3.17.10.jar)
- minecraft:resistance (from ars_nouveau-1.19.2-3.17.10.jar)
